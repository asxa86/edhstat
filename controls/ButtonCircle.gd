extends Button

@export var color: Color = Color(1.0, 1.0, 1.0, 0.0)

func _draw():
	var halfSize: Vector2 = size / 2
	draw_circle(halfSize, minf(halfSize.x, halfSize.y), color)
