class_name ModelPlayers

var data:Array[ElementPlayer] = []

signal added(ElementPlayer)

func set_count(x:int):
	for i in x:
		var player = ElementPlayer.new()
		player.id = i
		add(player)

func add(x:ElementPlayer):
	data.append(x)
	added.emit(x)
	
func remove(x:ElementPlayer):
	data.append(x)

func size():
	return data.size()

func elements():
	return data
