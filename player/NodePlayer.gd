class_name NodePlayer
extends ColorRect

var player: ElementPlayer = null
@onready var lblHealth:Label = %lblHealth
@onready var btnDecrement:Button = %btnDecrement
@onready var btnIncrement:Button = %btnIncrement

enum OrientationType {
	# Standard, upright, orientation.
	Standard,
	
	# 180 degree rotation, or flipped, orientation.
	Flipped
}

var orientationType:OrientationType = OrientationType.Standard

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if player != null:
		lblHealth.text = str(player.health)
	
	match orientationType:
		OrientationType.Flipped:
			set_pivot_offset(size / 2)
			set_rotation_degrees(180)

func set_element(x:ElementPlayer):
	player = x
	
func set_orientation_type(x:OrientationType):
	orientationType = x

func _on_btn_decrement_pressed():
	player.health -= 1

func _on_btn_increment_pressed():
	player.health += 1
