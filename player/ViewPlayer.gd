class_name ViewPlayer
extends GridContainer

var model: ModelPlayers = ModelPlayers.new()
@export var delegate:PackedScene
@export var colors:Array[Color] = ["red", "yellow", "blue", "green"]

# Called when the node enters the scene tree for the first time.
func _ready():
	model.added.connect(_on_model_added)
	model.set_count(4)

func _on_model_added(x:ElementPlayer):
	var node = delegate.instantiate()
	node.color = colors[x.id]
	node.set_element(x)
	
	if x.id < columns:
		node.set_orientation_type(NodePlayer.OrientationType.Flipped)
	
	add_child(node)
